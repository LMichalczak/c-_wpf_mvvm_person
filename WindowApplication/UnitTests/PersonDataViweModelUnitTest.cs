﻿using Castle.Core.Internal;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using WindowApplication.MessageBoxes;
using WindowApplication.Models;
using WindowApplication.ViewModels;

namespace UnitTests
{
    [TestClass]
    public class PersonDataViweModelUnitTest
    {
        #region IsPhoneNumberValid
        /// <summary>
        /// Method is checking whether the correct input is valid
        /// </summary>
        [TestMethod]
        public void IsPhoneNumberValid_OnlyDigits_True()
        {
            var mockMessageBox = Substitute.For<IMessageBoxOK>();
            var mockPersonData = Substitute.For<IPersonData>();
            var sut = new PersonDataViewModel(mockMessageBox, mockPersonData);
            string value = "012 34 56 789";

            var isValid = sut.IsPhoneNumberValid(value);

            Assert.IsTrue(isValid);
        }

        /// <summary>
        /// Method is checking whether the incorrect input is not valid
        /// </summary>
        [TestMethod]
        public void IsPhoneNumberValid_NotOnlyDigits_False()
        {
            var mockMessageBox = Substitute.For<IMessageBoxOK>();
            var mockPersonData = Substitute.For<IPersonData>();
            var sut = new PersonDataViewModel(mockMessageBox, mockPersonData);
            string value = ".12k4P* 7)9";

            var isValid = sut.IsPhoneNumberValid(value);

            Assert.IsFalse(isValid);
        }

        /// <summary>
        /// Method is checking whether correct phone number creates no error 
        /// </summary>
        [TestMethod]
        public void IsPhoneNumberValid_CorectInput_EmptyErrorDictionary()
        {
            var mockMessageBox = Substitute.For<IMessageBoxOK>();
            var mockPersonData = Substitute.For<IPersonData>();
            var sut = new PersonDataViewModel(mockMessageBox,mockPersonData);
            
            string value = "012 34 56 789";

            sut.IsPhoneNumberValid(value);
            
            Assert.IsTrue(sut.Errors.IsNullOrEmpty());
        }

        /// <summary>
        /// Method is checking whether correct phone number creates no error 
        /// </summary>
        [TestMethod]
        public void IsPhoneNumberValid_IncorrectInput_CreateErrorInDictionary()
        {
            var mockMessageBox = Substitute.For<IMessageBoxOK>();
            var mockPersonData = Substitute.For<IPersonData>();
            var sut = new PersonDataViewModel(mockMessageBox, mockPersonData);

            string value = "sgsd35/.,";

            sut.IsPhoneNumberValid(value);

            Assert.IsFalse(sut.Errors.IsNullOrEmpty());
            Assert.AreEqual(sut.Errors[nameof(sut.PhoneNumber)].Count, 1);
        }
        #endregion

        /// <summary>
        /// Method is checking whether messageBox is shown
        /// </summary>
        [TestMethod]
        public void Show_MessageBoxIsCreated()
        {
            var mockMessageBox = Substitute.For<IMessageBoxOK>();
            var mockPersonData = Substitute.For<IPersonData>();
            var sut = new PersonDataViewModel(mockMessageBox,mockPersonData);
            mockPersonData.getPersonData().Returns("AnyText");       

            sut.ShowPersonDataCommand.Execute(this);

            mockMessageBox.Received().Show(Arg.Any<string>(), Arg.Any<string>());
        }

       
    }
}
