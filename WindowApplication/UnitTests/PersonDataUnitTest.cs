﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WindowApplication.Models;

namespace UnitTests
{
    [TestClass]
    public class PersonDataUnitTest
    {
        [TestMethod]
        public void getPersonData_AllPropertiesSet_ReturnFourProperies()
        {
            //Arrange
            var mockPersonData = new PersonData
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Addres = "Addres",
                PhoneNumber = "123456789"
            };


            //Act
            var returnedText = mockPersonData.getPersonData();

            //Assert
            Assert.AreEqual(returnedText,"FirstName\nLastName\nAddres\n123456789");
        }
    }
}
