﻿namespace WindowApplication.MessageBoxes
{
    public interface IMessageBoxOK
    {
        void Show(string text, string caption);
    }
}
