﻿using System.Windows;

namespace WindowApplication.MessageBoxes
{
    class MessageBoxOK : IMessageBoxOK
    {
        public void Show(string text, string caption)
        {
            MessageBox.Show(text, caption, MessageBoxButton.OK);
        }
    }
}
