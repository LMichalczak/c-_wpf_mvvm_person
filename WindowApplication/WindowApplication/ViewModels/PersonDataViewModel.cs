﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using WindowApplication.Annotations;
using WindowApplication.MessageBoxes;
using WindowApplication.Models;

namespace WindowApplication.ViewModels
{
    public class PersonDataViewModel: INotifyPropertyChanged, IDataErrorInfo, IPersonDataViewModel
    {
        private readonly IPersonData personData;
        private readonly Dictionary<String, List<String>> errors;
        private const string PhoneNumber_ERROR = "Phone number can consist only of digits";
        private readonly IMessageBoxOK messageBoxOk;

        #region Properties
        public string FirstName
        {
            get { return personData.FirstName; }
            set
            {
                if (FirstName != value)
                {
                    personData.FirstName = value;
                    OnPropertyChanged("FirstName");
                }
            }
        }
        public string LastName
        {
            get { return personData.LastName; }
            set
            {
                if (LastName != value)
                {
                    personData.LastName = value;
                    OnPropertyChanged("LastName");
                }
            }
        }
        public string Addres
        {
            get { return personData.Addres; }
            set
            {
                if (Addres != value)
                {
                    personData.Addres = value;
                    OnPropertyChanged("Addres");
                }
            }
        }
        public string PhoneNumber
        {
            get { return personData.PhoneNumber; }
            set
            {
                if (IsPhoneNumberValid(value) && PhoneNumber != value)
                {
                    personData.PhoneNumber = value;
                    OnPropertyChanged("PhoneNumber");
                }
            }
        }

        /// <summary>
        /// IDataErrorInfo member
        /// </summary>
        public string Error { get; }

        public Dictionary<String, List<String>> Errors
        {
            get { return errors; }
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand ShowPersonDataCommand { get; private set; }
       

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region public methods
        /// <summary>
        /// Class constructor
        /// </summary>
        public PersonDataViewModel(IMessageBoxOK iMessageBoxOk, IPersonData iPersonData)
        {
            this.personData = iPersonData;
            this.ShowPersonDataCommand = new RelayCommand(ShowPersonData);
            errors = new Dictionary<string, List<string>>();
            this.messageBoxOk = iMessageBoxOk;
        }

        /// <summary>
        /// Show person data in message box
        /// </summary>
        public void ShowPersonData()
        {
            string message = personData.getPersonData();
            messageBoxOk.Show(message, "Personal Data");
        }

        /// <summary>
        /// Validates the PhoneNumber property, updating the errors collection as needed.
        /// </summary>
        /// <param name="value">TextBox input</param>
        /// <returns>Is PhoneNumber property valid</returns>
        public bool IsPhoneNumberValid(string value)
        {
            bool isValid = true;
            foreach (var c in value)
            {
                if ((c >= '0' && c <= '9') || c.Equals(' ')) continue;
                AddError("PhoneNumber", PhoneNumber_ERROR, false);
                isValid = false;
            }

            if (isValid)
            {
                RemoveError("PhoneNumber", PhoneNumber_ERROR);
            }

            return isValid;
        }

        /// <summary>
        /// Adds the specified error to the errors collection if it is not already present, 
        /// inserting it in the first position if isWarning is false. 
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="error"></param>
        /// <param name="isWarning"></param>
        public void AddError(string propertyName, string error, bool isWarning)
        {
            if (!errors.ContainsKey(propertyName))
                errors[propertyName] = new List<string>();

            if (!errors[propertyName].Contains(error))
            {
                if (isWarning) errors[propertyName].Add(error);
                else errors[propertyName].Insert(0, error);
            }
        }

        /// <summary>
        /// Removes the specified error from the errors collection if it is present. 
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="error"></param>
        public void RemoveError(string propertyName, string error)
        {
            if (errors.ContainsKey(propertyName) &&
                errors[propertyName].Contains(error))
            {
                errors[propertyName].Remove(error);
                if (errors[propertyName].Count == 0) errors.Remove(propertyName);
            }
        }

        public string this[string propertyName]
        {
            get
            {
                return (!errors.ContainsKey(propertyName) ? null :
                    String.Join(Environment.NewLine, errors[propertyName]));
            }
        }
        #endregion


    }
}
