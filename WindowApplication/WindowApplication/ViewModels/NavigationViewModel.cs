﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using WindowApplication.Annotations;
using WindowApplication.Views;

namespace WindowApplication.ViewModels
{
    public class NavigationViewModel : INotifyPropertyChanged
    {
        private object selectedUserControl;
        private List<UserControl> userControlList;
        private int viewListIndex;
        private bool backButtonIsEnable;
        private bool nextButtonIsEnable;

        /// <summary>
        /// Currently selected user control
        /// </summary>
        public object SelectedUserControl
        {
            get { return selectedUserControl; }
            set
            {
                if (selectedUserControl != value)
                {
                    selectedUserControl = value;
                    OnPropertyChanged("SelectedUserControl");
                }

            }
        }

        /// <summary>
        /// Property IsEnable of Back Button
        /// </summary>
        public bool BackButtonIsEnable
        {
            get { return backButtonIsEnable; }
            set
            {
                if (backButtonIsEnable != value)
                {
                    backButtonIsEnable = value;
                    OnPropertyChanged("BackButtonIsEnable");
                }

            }
        }

        /// <summary>
        /// Property IsEnable of Next Button
        /// </summary>
        public bool NextButtonIsEnable
        {
            get { return nextButtonIsEnable; }
            set
            {
                if (nextButtonIsEnable != value)
                {
                    nextButtonIsEnable = value;
                    OnPropertyChanged("NextButtonIsEnable");
                }

            }
        }

        /// <summary>
        /// List of user controls
        /// </summary>
        public List<UserControl> UserControlList
        {
            get { return userControlList; }
        }


        public ICommand NextCommand { get; private set; }
        public ICommand BackCommand { get; private set; }



        public NavigationViewModel()
        {
            this.NextCommand = new RelayCommand(Next);
            this.BackCommand = new RelayCommand(Back);
            viewListIndex = 0;
            userControlList = new List<UserControl> {new FirstNameView(), new LastNameView(), new AddresView(), new PhoneNumberView()};
            SelectedUserControl = UserControlList[0];
            BackButtonIsEnable = false;
            NextButtonIsEnable = true;
        }

        /// <summary>
        /// Method set IsEnable property of navigation buttons depending on index of current user control 
        /// </summary>
        private void ButtonsEnableChange()
        {
            NextButtonIsEnable = viewListIndex != UserControlList.Count-1;

            BackButtonIsEnable = viewListIndex != 0;
        }


        /// <summary>
        /// Method changes currently visible user control to the next one 
        /// </summary>
        public void Back()
        {
            SelectedUserControl = UserControlList[--viewListIndex];
            ButtonsEnableChange();
        }

        /// <summary>
        /// Method changes currently visible user control to the previous one 
        /// </summary>
        public void Next()
        {
            SelectedUserControl = UserControlList[++viewListIndex];
            ButtonsEnableChange();
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
