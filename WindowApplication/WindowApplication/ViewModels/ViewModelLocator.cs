﻿using CommonServiceLocator;
using GalaSoft.MvvmLight.Ioc;
using WindowApplication.MessageBoxes;
using WindowApplication.Models;

namespace WindowApplication.ViewModels
{
    class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            SimpleIoc.Default.Register<NavigationViewModel>();
            SimpleIoc.Default.Register<IMessageBoxOK>(() => new MessageBoxOK());
            SimpleIoc.Default.Register<IPersonData>(() => new PersonData());
            SimpleIoc.Default.Register<PersonDataViewModel>();
        }

        public PersonDataViewModel PersonDataViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<PersonDataViewModel>();
            }
        }

        public NavigationViewModel NavigationViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<NavigationViewModel>();
            }
        }
    }
}
