﻿using System.Windows.Input;

namespace WindowApplication.ViewModels
{
    public interface IPersonDataViewModel
    {
        void AddError(string propertyName, string error, bool isWarning);
        void RemoveError(string propertyName, string error);
        bool IsPhoneNumberValid(string value);
        void ShowPersonData();

        ICommand ShowPersonDataCommand { get; }
    }
}
