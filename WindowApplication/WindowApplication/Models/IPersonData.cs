﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowApplication.Models
{
    public interface IPersonData
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string Addres { get; set; }
        string PhoneNumber { get; set; }
        string getPersonData();
    }
}
