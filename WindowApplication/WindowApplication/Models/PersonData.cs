﻿namespace WindowApplication.Models
{
    /// <summary>
    /// Class contains personal data
    /// </summary>
    public class PersonData : IPersonData
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Addres { get; set; }
        public string PhoneNumber { get; set; }

        public string getPersonData()
        {
            string ret = string.Empty;
            ret += FirstName + '\n';
            ret += LastName + '\n';
            ret += Addres + '\n';
            ret += PhoneNumber;
            return ret;
        }
    }
}
